function btnNav() {
  var nav = document.getElementById("nav-overlay");
  if (nav.className === "overlay") {
    nav.className = "overlay open";
  } else {
    nav.className = "overlay";
  }
}
function btnClose() {
  var close = document.getElementById("nav-overlay");
  close.classList.remove("open");
}
